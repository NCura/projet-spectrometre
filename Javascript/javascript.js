// Fonction qui va allumer puis éteindre la LED intégré afin de savoir si la communication est établit
function test()
{
    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/test.bat",0,true);
}

// Donne le chemin où se trouve l'application html
var relpath = window.location.pathname.replace(/\\/g,'/').split('/');
relpath.pop();
relpath = relpath.join('/') + '/';

// Le moteur des fentes est un moteur pas à pas. L'ordre des pas est [4, 6, 5, 7]
// On initialise un compteur qui se situe sur le pas 4, donc dans la position 0 dans la liste
var pasCounterFenteEntree = 0;
// Taille de la fente d'entrée, on initialise à 0
var tailleFenteEntree = 0;

// Pareil pour la fente de sortie
var pasCounterFenteSortie = 0;
var tailleFenteSortie = 0;

// Pareil pour le moteur du réseau
var pasCounterReseau = 0;
var longueurOndeAncienne = 0;
var longueurOndeNouvelle = 0;

// Variable qui permet de savoir si on est sur le réseau 600 ou 1800
var traitsReseau = 600;

// Définie la taille de la fenêtre de l'application 
window.resizeTo(1100,850);

var text = "Étapes d'initialisation :";

// Fonction qui initialise le spectromètre
function Initialisation() {

    // On met à jour les valeurs de la taille de la fente dans l'application
    document.getElementById("fenteEntree").value = tailleFenteEntree;
    document.getElementById("fenteDeSortie").value = tailleFenteSortie;

    // On change le texte du button du menu pendant le temps que le spectromètre fasse sa séquence d'initialisation
    document.getElementById("initialisation-button").value = "Attendez...";
    // On désactive le bouton afin qu'on ne puisse pas reclicker dessus
    document.getElementById("initialisation-button").disabled = true;
    document.getElementById("initialisation-button").style.color = "white";

    text = text + "<br>Initialisation de la fente d'entrée : ...";
    document.getElementById("information").innerHTML = text;
    InitialisationFenteEntree();
    text = text + "OK";
    document.getElementById("information").innerHTML = text;

    text = text + "<br>Initialisation de la fente de sortie : ...";
    document.getElementById("information").innerHTML = text;
    InitialisationFenteSortie();
    text = text + "OK";
    document.getElementById("information").innerHTML = text;

    // On met la sortie sur l'axe
    text = text + "<br>Initialisation du miroir de sortie : ...";
    document.getElementById("information").innerHTML = text;
    MiroirSortieFermeture();
    text = text + "OK";
    document.getElementById("information").innerHTML = text;

    text = text + "<br>Initialisation du reśeau : ...";
    document.getElementById("information").innerHTML = text;

    // On met le réseau sur 600 traits/mm
    RotationReseauVers600();

    // On met le reseau dans la position 0
    InitialisationReseau();
    text = text + "OK";
    document.getElementById("information").innerHTML = text;

    text = text + "<br><br>Finalisation...";
    document.getElementById("information").innerHTML = text;
    // Au bout de 3 secondes, on passe de la fenêtre menu à la fenêtre programme
    setTimeout(function (){
        document.getElementById("initialisation-div").style.display = "none";
        document.getElementById("programme-div").style.display = "block";
        // On rechange le texte du button du menu
        document.getElementById("initialisation-button").value = "Initialisation";
        // Et on fait en sorte que le bouton puisse être reclicker à nouveau
        document.getElementById("initialisation-button").disabled = false;
    }, 3000);
}

// Button permettant de revenir sur la fenêtre menu
function Menu() {
    document.getElementById("information").innerHTML = "Étapes d'initialisation :";
    document.getElementById("initialisation-div").style.display = "block";
    document.getElementById("programme-div").style.display = "none";
}

// Lire documentation
function InitialisationFenteEntree() {
    var shell1 = new ActiveXObject("WScript.Shell");
    var shell2 = new ActiveXObject("WScript.Shell");

    // On va en buter
    shell1.run(relpath + "Moteurs/FermerFenteEntree.bat",0,true);
    // On ouvrer jusqu'à que la fente soit au maximum ouverte mais toujours sans lumière qui passe : position 0
    shell2.run(relpath + "Moteurs/OuvrirFenteEntree.bat",0,true);

    // La fente est donc en position 0
    tailleFenteEntree = 0;
}

// Lire documentation
function InitialisationFenteSortie() {
    var shell1 = new ActiveXObject("WScript.Shell");
    var shell2 = new ActiveXObject("WScript.Shell");

    // On va en buter
    shell1.run(relpath + "Moteurs/FermerFenteSortie.bat",0,true);
    // On ouvrer jusqu'à que la fente soit au maximum ouverte mais toujours sans lumière qui passe : position 0
    shell2.run(relpath + "Moteurs/OuvrirFenteSortie.bat",0,true);

    // La fente est donc en position 0
    tailleFenteSortie = 0;
}

function MiroirSortieOuverture() {
    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/MiroirSortieSens1.bat",0,true);
    document.getElementById("miroir-sortie-texte").innerHTML = "Sortie sur le latéral.";
    document.getElementById("MiroirSortieOuverture").style.display = "none";
    document.getElementById("MiroirSortieFermeture").style.display = "inline-block";
}
function MiroirSortieFermeture() {
    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/MiroirSortieSens2.bat",0,true);
    document.getElementById("miroir-sortie-texte").innerHTML = "Sortie sur l'axe.";
    document.getElementById("MiroirSortieOuverture").style.display = "inline-block";
    document.getElementById("MiroirSortieFermeture").style.display = "none";
}

function RotationReseauVers600() {
    traitsReseau = 600;
    document.getElementById("reseau-rotation-texte").innerHTML = "Le réseau est en 600 traits/mm.";
    document.getElementById("reseauVers600").style.display = "none";
    document.getElementById("reseauVers1800").style.display = "inline-block";

    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/RotationReseauSens1.bat",0,true);
}

function RotationReseauVers1800() {
    traitsReseau = 1800;
    document.getElementById("reseau-rotation-texte").innerHTML = "Le réseau est en 1800 traits/mm.";
    document.getElementById("reseauVers600").style.display = "inline-block"
    document.getElementById("reseauVers1800").style.display = "none"

    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/RotationReseauSens2.bat",0,true);
}

function InitialisationReseau() {
    longueurOndeAncienne = 500;
    longueurOndeNouvelle = 500;

    var shell = new ActiveXObject("WScript.Shell");
    shell.run(relpath + "Moteurs/InitialisationReseau.bat",0,true);
}

function FenteSortie() {
    // Pas possibles
    listeDePas = [a, c, b, d];
    
    // Pas dans lequel on se trouve
    pas = listeDePas[pasCounterFenteSortie];

    // Taille de la fente que l'on souhaite
    $taille = document.getElementById("fenteDeSortie").value;

    // Si la taille est négatif on remet la taille qui avait avant
    if ($taille < 0) {
        $taille = tailleFenteSortie;
    // Si la taille supérieure à la valeur maximale, on la met à la valeur maximale
    } else if ($taille > 2000) {
        $taille = 2000;
    }

    // Taille d'un pas
    taillePas = 13;

    // On arrondi la taille entrée au multiple du pas le plus proche
    if ($taille%taillePas <= taillePas/2) {
        $taille = $taille - $taille%taillePas;
    } else {
        $taille = parseInt($taille, 10) + parseInt(taillePas-$taille%taillePas, 10);
    }
    
    // On met à jour la valeur de la taille de la fente dans l'interface
    document.getElementById("fenteDeSortie").value = $taille;

    // Nombre de cycles qu'on doit effectuer afin d'ouvrir la fente à la taille donnée
    if (tailleFenteSortie <= $taille) {
        nombreDePas = Math.ceil(($taille-tailleFenteSortie)/taillePas);
        // Sens dans lequel il faut faire tourner le moteur : Ouverture de la fente
        sens = 0;
    } else {
        nombreDePas = Math.ceil((tailleFenteSortie-$taille)/taillePas);
        // Sens dans lequel il faut faire tourner le moteur : Fermeture de la fente
        sens = 1;
    }
    
    var shell = new ActiveXObject("WScript.Shell");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    // On ouvre le fichier pas.ps1, où l'on va écrire la séquence à faire pour le moteur
    var s = fso.OpenTextFile(relpath + "Moteurs/pas.ps1", 2, true);
    // On commance avec les commandes d'habitude afin d'ouvrir le port
    s.WriteLine("$port = new-Object System.IO.Ports.SerialPort COM4,9600,None,8,one");
    s.WriteLine("$port.open()");
    
    // Si on doit ouvrir la fente
    if (sens == 0) {
        // On fait le nombre de pas nécessaires
        for (var i = 1; i < nombreDePas; i++) {
            // On écrit la séquence correspondante à ce pas
            s.WriteLine("$port.WriteLine('" + pas + "')");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine('" + pas + "')");
            // On passe au prochain pas
            pasCounterFenteSortie = (pasCounterFenteSortie+1)%4;
            pas = listeDePas[pasCounterFenteSortie];
        }
    // Si on doit fermer la fente
    } else {
        // On fait le nombre de cycles nécessaires
        for (var i = 1; i < nombreDePas; i++) {
            // On écrit la séquence correspondante à ce pas
            s.WriteLine("$port.WriteLine(" + pas + ")");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine(" + pas + ")");
            // On passe au prochain pas
            pasCounterFenteSortie = Math.abs((pasCounterFenteSortie-1))%4;
            pas = listeDePas[pasCounterFenteSortie];
        }
    }

    // On fini par fermer le port
    s.WriteLine("$port.close()");
    // Puis par fermer le fichier
    s.Close();
    // Et ensuite on l'éxecute
    shell.run(relpath + "Moteurs/pas.bat",0,true);

    // On enregistre la nouvelle taille de la fente
    if ($taille != "NaN") {
        tailleFenteSortie = parseInt($taille);
    }
}

// Mêmes commentaires que pour la fente de sortie
function FenteEntree() {
    listeDePas = [6, 8, 7, 9];
    
    pas = listeDePas[pasCounterFenteEntree];

    $taille = document.getElementById("fenteEntree").value;

    if ($taille < 0) {
        $taille = tailleFenteEntree;
    } else if ($taille > 2000) {
        $taille = 2000;
    }

    taillePas = 13;

    if ($taille%taillePas <= taillePas/2) {
        $taille = $taille - $taille%taillePas;
    } else {
        $taille = parseInt($taille, 10) + parseInt(taillePas-$taille%taillePas, 10);
    }
    
    document.getElementById("fenteEntree").value = $taille;

    if (tailleFenteEntree <= $taille) {
        nombreDePas = Math.ceil(($taille-tailleFenteEntree)/taillePas);
        sens = 0;
    } else {
        nombreDePas = Math.ceil((tailleFenteEntree-$taille)/taillePas);
        sens = 1;
    }

    var shell = new ActiveXObject("WScript.Shell");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var s = fso.OpenTextFile(relpath + "Moteurs/pas.ps1", 2, true);
    s.WriteLine("$port = new-Object System.IO.Ports.SerialPort COM4,9600,None,8,one");
    s.WriteLine("$port.open()");
    
    if (sens == 0) {
        for (var i = 1; i < nombreDePas; i++) {
            s.WriteLine("$port.WriteLine(" + pas + ")");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine(" + pas + ")");
            pasCounterFenteEntree = (pasCounterFenteEntree+1)%4;
            pas = listeDeCycles[pasCounterFenteEntree];
        }
    } else {
        for (var i = 1; i < nombreDePas; i++) {
            s.WriteLine("$port.WriteLine(" + pas + ")");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine(" + pas + ")");
            pasCounterFenteEntree = Math.abs((pasCounterFenteEntree-1))%4;
            pas = listeDePas[pasCounterFenteEntree];
        }
    }

    s.WriteLine("$port.close()");
    s.Close();
    shell.run(relpath + "Moteurs/pas.bat",0,true);

    if ($taille != "NaN") {
        tailleFenteEntree = parseInt($taille);
    }
}

// Mêmes commentaires que pour les fentes
function TranslationReseau()
{
    listeDePas = [e, g, f, h];
    
    pas = listeDePas[pasCounterReseau];

    // Longueur d'onde souhaité
    $taille = document.getElementById("reseau").value;

    if ($taille < 400) {
        $taille = 400;
    } else if ($taille > 1000) {
        $taille = 1000;
    }

    // On transforme la longueur d'onde en distance de translation
    $taille = Math.asin(Math.pow(10, -6)*traitsReseau * $taille/(2*Math.cos(16.5/2))) - 16.5*Math.PI/360;

    taillePas = 1;

    if ($taille%taillePas <= taillePas/2) {
        $taille = $taille - $taille%taillePas;
    } else {
        $taille = parseInt($taille, 10) + parseInt(taillePas-$taille%taillePas, 10);
    }
    
    document.getElementById("reseau").value = $taille;

    if (tailleReseau <= $taille) {
        nombreDePas = Math.ceil(($taille-tailleReseau)/taillePas);
        sens = 0;
    } else {
        nombreDePas = Math.ceil((tailleReseau-$taille)/taillePas);
        sens = 1;
    }
    
    var shell = new ActiveXObject("WScript.Shell");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var s = fso.OpenTextFile(relpath + "Moteurs/pas.ps1", 2, true);
    s.WriteLine("$port = new-Object System.IO.Ports.SerialPort COM4,9600,None,8,one");
    s.WriteLine("$port.open()");

    if (sens == 0) {
        for (var i = 1; i < nombreDePas; i++) {
            s.WriteLine("$port.WriteLine(" + pas + ")");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine(" + pas + ")");
            pasCounterReseau = (pasCounterReseau+1)%4;
            pas = listeDePas[pasCounterReseau];
        }
    } else {
        for (var i = 1; i < nombreDePas; i++) {
            s.WriteLine("$port.WriteLine(" + pas + ")");
            s.WriteLine("Start-Sleep -m 3");
            s.WriteLine("$port.WriteLine(" + pas + ")");
            pasCounterReseau = Math.abs((pasCounterReseau-1))%4;
            pas = listeDePas[pasCounterReseau];
        }
    }

    s.WriteLine("$port.close()");
    s.Close();
    shell.run(relpath + "Moteurs/pas.bat",0,true);

    if ($taille != "NaN") {
        tailleReseau = parseInt($taille);
    }
}