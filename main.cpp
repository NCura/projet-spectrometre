#include "mbed.h"

Serial pc(SERIAL_TX, SERIAL_RX);

// Led du microcontroleur
// Utilisé pour vérifier la communication entre PC et carte
DigitalOut led(LED1);

// Miroir sortie
// Pin PF_2 -> char 2
DigitalOut M3M(PF_2);
// Pin PA_7 -> char 3
DigitalOut M3P(PA_7);

// Rotation reseau
// Pin PF_3 -> char 4
DigitalOut M4P(PF_3);
// Pin PF_10 -> char 5
DigitalOut M4M(PF_10);

// Fente Entree
// Pin PF_8 -> char 6
DigitalOut M11(PF_8);
// Pin PF_7 -> char 7
DigitalOut M13(PF_7);
// Pin PF_9 -> char 8
DigitalOut M14(PF_9);
// Pin PG_1 -> char 9
DigitalOut M16(PG_1);

// Fente Sortie
// Pin PG_0 -> char a
DigitalOut M21(PG_0);
// Pin PD_1 -> char b
DigitalOut M23(PD_1);
// Pin PD_0 -> char c
DigitalOut M24(PD_0);
// Pin PH_0 -> char d
DigitalOut M26(PH_0);

// Translation reseau
// Pin PF_5 -> char e
DigitalOut M5B1P(PF_5);
// Pin PA_3 -> char f
DigitalOut M5B1M(PA_3);
// Pin PC_0 -> char g
DigitalOut M5B2P(PC_0);
// Pin PC_3 -> char h
DigitalOut M5B2M(PC_3);

// Bouton translation reseau
DigitalIn Ctrl(PC_8);

int mycount = 0;
int mycount2 = 0;

int main() 
{
    // On met le control du bouton du moteur de translation de reseau en mode pull up
    Ctrl.mode(PullUp);
    
    // On initialise toutes les broches pour plus de sécurité
    // Led du microcontroleur
    led = 0;
    // Miroir sortie
    M3M = 0;
    M3P = 0;
    // Rotation réseau
    M4M = 0;
    M4P = 0;
    // Fente entrée
    M11 = 0;
    M13 = 0;
    M14 = 0;
    M16 = 0;
    // Fente sortie
    M21 = 0;
    M23 = 0;
    M24 = 0;
    M26 = 0;
    // Translation réseau
    M5B1P = 0;
    M5B1M = 0;
    M5B2P = 0;
    M5B2M = 0;
    
    while (true) {
        
        // Caractère renvoie par l'ordinateur au microcontroleur
        char c = pc.getc();
        
        switch(c)
        {
            // Led microcontroleur
            case 't' :
                led = !led;
                break;
            // Moteur 1 : Fente entrée
            case '6' :
                M11 = !M11;
                break;
            case '7' :
                M13 = !M13;
                break;
            case '8' :
                M14 = !M14;
                break;
            case '9' :
                M16 = !M16;
                break;
            // Moteur 2 : Fente sortie
            case 'a' :
                M21 = !M21;
                break;
            case 'b' :
                M23 = !M23;
                break;
            case 'c' :
                M24 = !M24;
                break;
            case 'd' :
                M26 = !M26;
                break;
            // Moteur 3 : Miroir sortie
            case '2' :
                M3M = !M3M;
                break;
            case '3' :
                M3P= !M3P;
                break;
            // Moteur 4 : Rotation reseau
            case '4' :
                M4P = !M4P;
                break;
            case '5' :
                M4M = !M4M;
                break;
            // Moteur 5 : Translation Moteur
            case 'e' :
                M5B1P = !M5B1P;
                break;
            case 'f' :
                M5B1M = !M5B1M;
                break;
            case 'g' :
                M5B2P = !M5B2P;
                break;
            case 'h' :
                M5B2M = !M5B2M;
                break;
                
            // Initialisation moteur translation reseau
            case '1' :
                M5B1P = 1;
                wait_ms(4);
                while (Ctrl)
                {
                    M5B2P = 1;
                    wait_ms(4);
                    M5B1P = 0;
                    wait_ms(4);
                    M5B1M = 1;
                    wait_ms(4);
                    M5B2P = 0;
                    wait_ms(4);
                    M5B2M = 1;
                    wait_ms(4);
                    M5B1M = 0;
                    wait_ms(4);
                    M5B1P = 1;
                    wait_ms(4);
                    M5B2M = 0;
                    wait_ms(4);
                }
                M5B1P = 0;
                
                M5B2M = 1;
                wait_ms(4);
                while (mycount2 < 500) 
                {
                    mycount2 = mycount2 + 1;
                    M5B1M = 1;
                    wait_ms(4);
                    M5B2M = 0;
                    wait_ms(4);
                    M5B2P = 1;
                    wait_ms(4);
                    M5B1M = 0;
                    wait_ms(4);
                    M5B1P = 1;
                    wait_ms(4);
                    M5B2P = 0;
                    wait_ms(4);
                    M5B2M = 1;
                    wait_ms(4);
                    M5B1P = 0;
                    wait_ms(4);
                }
                mycount2 = 0;
                break;
        }
    }
}